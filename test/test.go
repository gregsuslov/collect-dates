package main

import (
	//    "bufio"
	//	"encoding/csv"
	"fmt"
	"log"
	"io/ioutil"
)

const dirName = "csv"

type Record struct {
	Date  uint32
	Int1  int32
	Int2  int32
	Floa  float32
	Up    uint32
	Left  uint32
	Right uint32
}

func main() {
	var rec Record
	fmt.Println(rec)
	files, err := ioutil.ReadDir(dirName)
	fatal(err)
	for _, file := range files {
		fmt.Println(file)
	}
	readCSV("csv/file1.csv")
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
