// collect-dates тестовая программа
// На вход от пользователя поступает имя директории (с путем). 
// В директории расположено множество cvs файлов. 
// Каждый файл содержит столбец с датой, и несколько измерений
// Например: date; A; B; C
// Размер данных таков, что ни исходные файлы, ни даже результат 
// аггрегации (т.к. дат может быть много) не поместятся в память.
// A и B типа uint64, C типа float Файлы желательно обрабатывать
// параллельно, для ускорения процесса. В случае обнаружения
// некорректной строки - необходимо полностью прервать обработку
// всех файлов и вывести имя файла и номер строки с ошибкой. 

// Путь к директори задается первым аргументом командной строки
// При отсутствии этого параметра будет использоваться
// тестовая директория test, расположенная в текущей директории
// Если вторым аргументом задать p (строчная латинская),
// то будет производиться параллельная обработка 

// Григорий Суслов, 2018/06/18-20


package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
	"sync"
)

const (
	fileExt  = "csv"	// расширение входных файлов
	divider = ';'		// разделитель полей в файлах
)
var (
	tempFile *os.File		// Временный бинарный файл
	err error
	dirName string = "test"
	parallel bool = false
)

func main() {
	from := time.Now()
	if len(os.Args) > 1 {
		dirName = os.Args[1]
	}
	if len(os.Args) > 2 {
		parallel = os.Args[2] == "p"
	}
	tempFile, err = os.Create("temp.$$$")
	fatal(err)
	defer delTemp()
	files, err := ioutil.ReadDir(dirName)
	fatal(err)
	if parallel {
		var wg sync.WaitGroup 
		wg.Add(len(files))
		parRead := func(file os.FileInfo) {
			defer wg.Done()
			readCSV(file)
		}
		for _, file := range files {
			go parRead(file)
		}
		wg.Wait()
	} else {
		for _, file := range files {
			readCSV(file)
		}
	}
	showResult()
	fmt.Println("Duration "+time.Since(from).String())
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// delTemp() удаление временного файла после окончания выполнения программы
func delTemp() {
	tempFile.Close()
	os.Remove("temp.$$$")
}
