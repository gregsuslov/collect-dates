package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"sync"
)

type Record struct {
	Date  uint32 // преобразованная дата
	Int1  uint64 // три измерения
	Int2  uint64
	Real  float64
	Left  uint32 // адрес записи с предыдущей датой
	Right uint32 // адрес записи с последующим указателем
}

const recLen = 3*8 + 3*4 // размер записи типа Record

var (
	treeDepth uint32 // максимальная глубина дерева записей
	mu        sync.Mutex
	headRec   Record     // первая запись в файле (для ускорения чтения)
	fileSize  uint32 = 0 // текущий размер файла
)

// saveRecord(rec Record) сохраняет запись во временном файле
func saveRecord(rec Record) {
	mu.Lock()
	defer mu.Unlock()
	if fileSize == 0 { // первая запись
		putRec(rec, 0, true)
		treeDepth = 1
		return
	}
	var lastPos uint32
	prevRec := headRec
	var depth uint32 = 1
	for {
		if prevRec.Date == rec.Date { // запись с такой датой уже существует
			prevRec.Int1 += rec.Int1
			prevRec.Int2 += rec.Int2
			prevRec.Real += rec.Real
			putRec(prevRec, lastPos, false)
			break
		} else if prevRec.Date < rec.Date { // уход на правую ветвь
			if prevRec.Right != 0 {
				lastPos = prevRec.Right
				prevRec = getRec(lastPos)
				depth++
				continue
			}
			prevRec.Right = fileSize
		} else { // уход на левую ветвь
			if prevRec.Left != 0 {
				lastPos = prevRec.Left
				prevRec = getRec(lastPos)
				depth++
				continue
			}
			prevRec.Left = fileSize
		}
		putRec(prevRec, lastPos, false) // изменение ссылки на следующую запись
		putRec(rec, fileSize, false)    // сохранение текущей записи
		depth++
		break
	}
	if depth > treeDepth {
		treeDepth = depth
	}
}

// showResult() вывод результата
func showResult() {
	fmt.Println("   date       A    B    C")
	slice := make([]Record, treeDepth)
	dup := make([]bool, treeDepth) // для проверки возвращения на запись справа
	slice[0] = headRec
	var depth int = 0
	for {
		if slice[depth].Left == 0 {
			if !dup[depth] {
				printRec(slice[depth])
			}
			if slice[depth].Right == 0 {
				if depth == 0 {
					return
				}
				dup[depth] = false
				depth--
			} else {
				dup[depth] = true
				depth++
				slice[depth] = getRec(slice[depth-1].Right)
				slice[depth-1].Right = 0
			}
		} else {
			depth++
			slice[depth] = getRec(slice[depth-1].Left)
			slice[depth-1].Left = 0
		}
	}
}

// putRec(rec Record, ofs uint32, first bool) помещает новую запись
// во врменный файл или изменяет headRec (если ofs == 0)
func putRec(rec Record, ofs uint32, first bool) {
	if ofs == fileSize {
		fileSize += recLen
	}
	if ofs == 0 {
		headRec = rec
		if !first {
			return
		}
	}
	_, err := tempFile.Seek(int64(ofs), 0)
	fatal(err)
	buf := new(bytes.Buffer)
	//	buf := bytes.NewBuffer(make([]byte, 26))
	err = binary.Write(buf, binary.BigEndian, &rec)
	fatal(err)
	_, err = tempFile.Write(buf.Bytes())
	fatal(err)
}

// getRec(ofs uint32) считывает запись из временного файла с позиции ofs
func getRec(ofs uint32) Record {
	if ofs == 0 {
		return headRec
	}
	_, err := tempFile.Seek(int64(ofs), 0)
	fatal(err)
	var rec Record
	buf := bytes.NewBuffer(make([]byte, recLen))
	_, err = tempFile.Read(buf.Bytes())
	fatal(err)
	err = binary.Read(buf, binary.BigEndian, &rec)
	fatal(err)
	return rec
}

func printRec(rec Record) {
	year := rec.Date / 10000
	mont := rec.Date % 10000 / 100
	day := rec.Date % 100
	fmt.Printf("%4d-%02d-%02d:%4d,%4d,%6.2f\n", year, mont, day, rec.Int1, rec.Int2, rec.Real)
}
