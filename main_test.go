package main

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestParseDate(t *testing.T) {
	tests := []struct {
		Date string
		Res  uint32
	}{
		{"2018-01-02", 20180102},
		{"20180102", 0},
		{"2018-13-05", 0},
		{"2018-12-32", 0},
		{"2018-04-31", 0},
		{"2018-02-29", 0},
		{"2020-02-29", 20200229},
		{"2018/03/07", 0},
	}
	for _, test := range tests {
		res := parseDate(test.Date)
		if res != test.Res {
			t.Errorf("For date `%s` got: %d, wanted: %d.", test.Date, res, test.Res)
		}
	}
}

func TestParseLine(t *testing.T) {
	tests := []struct {
		Line []string
		Res  Record
	}{
		{Line: []string{"2018-01-02", "5", "10", "2.5"},
			Res: Record{Date: 20180102, Int1: 5, Int2: 10, Real: 2.5}},
		{Line: []string{"5", "10", "2.5"},
			Res: Record{Date: 0}},
		{Line: []string{"2018-01-02", "5+", "10", "2.5"},
			Res: Record{Date: 0}},
		{Line: []string{"2018-01-02", "5", "10x", "2.5"},
			Res: Record{Date: 0}},
		{Line: []string{"2018-01-02", "5", "10", "2,5"},
			Res: Record{Date: 0}},
	}
	for _, test := range tests {
		res := parseLine(test.Line)
		if res != test.Res {
			t.Errorf("For line [%s] got: %v, wanted %v", strings.Join(test.Line, ", "), res, test.Res)
		}
	}
}

func TestReadCSV(t *testing.T) {
	recs := []Record{
		{20180301, 12, 16, 12.0, 108, 36},
		{20180302, 10, 14, 12.12, 0, 72},
		{20180305, 2, 4, 2.12, 288, 180},
		{20180226, 2, 3, 4.5, 216, 144},
		{20180227, 3, 4, 5.3, 0, 0},
		{20180425, 1, 0, 0.3, 0, 252},
		{20180215, 0, 15, 1.5, 0, 0},
		{20180429, 3, 25, -2.5, 0, 0},
		{20180304, 1, 5, 2.5, 0, 0},
	}
	tempFile, err = os.Create("temp.$$$")
	fatal(err)
	defer delTemp()
	files, err := ioutil.ReadDir(dirName)
	fatal(err)
	for _, file := range files {
		readCSV(file)
	}
	var pos uint32 = 0
	for i, wait := range recs {
		rec := getRec(pos)
		pos += recLen
		if rec != wait {
			t.Errorf("For record #%d got: %v, wanted %v", i, rec, wait)
		}
	}
}
