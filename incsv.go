package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

const dateLen = 10

// readCSV(file os.FileInfo) чтение записей из файла .csv
// и вызов функции для сохранения записей во временном файле
func readCSV(file os.FileInfo) {
	fname := file.Name()
	parts := strings.SplitAfter(fname, ".")
	last := len(parts) - 1
	if last == 0 || file.IsDir() || strings.ToLower(parts[last]) != fileExt {
		return // если файл не является файлом .csv
	}
	csvFile, err := os.Open(dirName + "/" + fname)
	fatal(err)
	defer csvFile.Close()
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.Comma = divider
	for i := 1; ; i++ {
		line, err := reader.Read()
		if err == io.EOF {
			break		// файл прочитан полностью
		}
		if i == 1 {
			continue	// певая запись с заголовком пропускается
		}
		rec := parseLine(line)
		// генерируется ошибка, если запись не распарсивается
		if err != nil || rec.Date == 0 {
			err = fmt.Errorf("Error: file `%[1]s`, line %[2]d", fname, i)
			log.Fatal(err)
		}
		saveRecord(rec)		// запись сохраняется во временном файле
	}
}

// преобразование текстовой строки в структуру типа Record
// если обнаружен неверный формат, то Record.Date = 0
func parseLine(line []string) Record {
	var rec Record
	if len(line) != 4 {
		return rec
	}
	i1, err := strconv.ParseUint(strings.TrimSpace(line[1]), 10, 64)
	if err != nil {
		return rec
	}
	i2, err := strconv.ParseUint(strings.TrimSpace(line[2]), 10, 64)
	if err != nil {
		return rec
	}
	f, err := strconv.ParseFloat(strings.TrimSpace(line[3]), 64)
	if err != nil {
		return rec
	}
	return Record{
		Date: parseDate(line[0]),
		Int1: i1,
		Int2: i2,
		Real: f,
	}
}

// Преобразрвание даты вида yyyy-mm-dd в целое uint32
func parseDate(date string) uint32 {
	if len(date) != dateLen {
		return 0
	}
	if date[4] != '-' || date[7] != '-' {
		return 0
	}
	year, err := strconv.ParseUint(date[:4], 0, 32)
	if err != nil {
		return 0
	}
	month, err := strconv.ParseUint(date[5:7], 0, 32)
	if err != nil || month == 0 || month > 12 {
		return 0
	}
	day, err := strconv.ParseUint(date[8:], 0, 32)
	if err != nil || day == 0 || day > 31 {
		return 0
	}
	if (month == 4 || month == 6 || month == 9 || month == 11) && day == 31 {
		return 0
	}
	if month == 2 {
		if day > 29 {
			return 0
		}
		if year % 4 != 0 && day == 29 {
			return 0
		}
	}
	return uint32(year)*10000 + uint32(month)*100 + uint32(day)
}
